## RQUIREMENTS

1. At least a "dual core" CPU and 4GB or above ram

2. Latest version of "node js" installed {{===>>> Learn more `https://nodejs.org/en/`}}

3. Yarn package manager recommended [You can enable it via the command `corepack enable`] {{===>>> Learn more `https://yarnpkg.com/`}}

## SCRIPT COMMANDS

0. node testCrawler.js {{===>>> "Tests the antibot stealth"}}

1. node generateCombinations.js [number_of_combinations] {{===>>> "Generates combination files"}}

2. node crawler.js [combination_file_name] [number_of_records](optional => if not specified will work with all the records/combinations)
