const fs = require("fs");
const csvWriter = require("csv-writer").createObjectCsvWriter;

// Define the characters to use
const CHARACTERS = "abcdefghijklmnopqrstuvwxyz";

// Define the maximum number of characters per combination
let maxLength;

// Define directory to store the combinations
const DIR = "./combinations";

// Main function
function main() {
  if (!process.argv[2]) {
    console.log(
      "Please specify a maximum length for the combinations. Learn more in README.md"
    );
    return;
  }

  maxLength = parseInt(process.argv[2]);

  if (maxLength > 5) {
    console.log("Maximum length must be less than 5");
    return;
  }

  if (!fs.existsSync(DIR)) {
    fs.mkdirSync(DIR);
  }

  // Generate all possible combinations
  let combinations = [];

  for (let i = 1; i <= maxLength; i++) {
    for (let j = 0; j < CHARACTERS.length; j++) {
      let prefix = CHARACTERS[j];
      if (i === 1) {
        combinations.push(prefix);
      } else {
        let suffixes = combinations.filter((c) => c.length === i - 1);
        suffixes.forEach((suffix) => {
          combinations.push(prefix + suffix);
        });
      }
    }
  }

  // Write each combination to its respective CSV file in the new directory
  for (let i = 0; i < CHARACTERS.length; i++) {
    let character = CHARACTERS[i];
    let filename = `${character}.csv`;

    const csvWriterObj = csvWriter({
      path: `combinations/${filename}`,
      header: [
        { id: "combination", title: "combination" },
        { id: "status", title: "status" },
      ],
    });

    let data = [];

    combinations.forEach((combination) => {
      if (combination[0] === character) {
        data.push({ combination: combination, status: "not_done" });
      }
    });

    csvWriterObj
      .writeRecords(data)
      .then(() => {
        console.log(`Generated ${filename} with ${data.length} combinations.`);
      })
      .catch((err) => {
        console.error(`Failed to generate ${filename}: ${err}`);
      });
  }
}

main();
