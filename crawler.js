const fs = require("fs");
const path = require("path");
const csv = require("csv-parser");
const csvWriter = require("csv-writer").createObjectCsvWriter;
const puppeteer = require("puppeteer-extra");
const cliProgress = require("cli-progress");
const colors = require("ansi-colors");

const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

// create new progress bar
const multibar = new cliProgress.MultiBar({
  format:
    colors.green("{bar}") + " | {filename} | {value}/{total} | {percentage}%",
  barCompleteChar: "\u2588",
  barIncompleteChar: "\u2591",
  hideCursor: true,
});

let allCsvRecords = [];
let notDoneCsvRecords = [];
let targetedFile = process.argv[2];
let targetedRecordNumbers = process.argv[3];
let results = [];

const csvWriterOutputObj = csvWriter({
  path: `outputs/output_${targetedFile}.csv`,
  header: [
    { id: "encryptedUid", title: "encryptedUid" },
    { id: "nickname", title: "nickname" },
    { id: "followerCount", title: "followerCount" },
    { id: "userPhotoUrl", title: "userPhotoUrl" },
  ],
});

const csvWriterObj = csvWriter({
  path: `combinations/${targetedFile}.csv`,
  header: [
    { id: "combination", title: "combination" },
    { id: "status", title: "status" },
  ],
});

if (!targetedFile) {
  console.log("Please specify the targeted file. Learn more in README.md");
  process.exit(1);
}

// Read the targeted file and store in the global variables
fs.createReadStream(path.join(__dirname, "combinations", `${targetedFile}.csv`))
  .pipe(csv())
  .on("data", (data) => {
    allCsvRecords.push(data);
    if (data.status === "not_done") {
      // Push to notDoneCsvRecords
      notDoneCsvRecords.push(data);
    }
  })
  .on("end", () => {
    console.log("Done reading the targeted file. Initiating crawler...");
    main();
  });

async function main() {
  let workingTreeRecords;

  if (!targetedRecordNumbers) {
    workingTreeRecords = notDoneCsvRecords;
    console.log(
      `No record limit specified. Using all ${notDoneCsvRecords.length} combination records.`
    );
  } else {
    workingTreeRecords = allCsvRecords.slice(
      0,
      parseInt(targetedRecordNumbers)
    );
    console.log(`Limiting to ${targetedRecordNumbers} combination records.`);
  }

  try {
    console.log("=========================================================");
    console.log(
      `Starting crawler for ${workingTreeRecords.length} combination records.`
    );
    console.log("=========================================================");

    // Lanuch instance
    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox"],
      // defaultViewport: false, // default viewport width and height are reducted
      userDataDir: "./tmp",
    });

    const page = await browser.newPage();
    await page.setUserAgent(
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
    );
    await page.setViewport({ width: 983, height: 921 });
    await page.goto(
      "https://www.binance.com/en/futures-activity/leaderboard/futures"
    );

    const b1 = multibar.create(workingTreeRecords.length, 0, {
      filename: `${targetedFile}.csv`,
    });
    const b2 = multibar.create(workingTreeRecords.length, 0, {
      filename: `updates on output_${targetedFile}.csv`,
    });

    // Perform network interception
    await page.setRequestInterception(true);

    page.on("request", (request) => request.continue());

    // Extract data from response
    page.on("response", async (response) => {
      if (response.url().includes("searchNickname")) {
        const res = await page.waitForResponse((response) =>
          response.url().includes("searchNickname")
        );
        const responseData = await response.json();

        // console.log(responseData.success);

        if (
          responseData?.success &&
          responseData?.data &&
          responseData?.data?.length > 0
        ) {
          // Check if the record already exists or not
          const exists = results.findIndex(
            (t) => t.nickname === responseData.data[0].nickname
          );
          // console.log("ran here");

          if (exists === -1) {
            // console.log("ran");
            results.push(...responseData.data);

            // Update data records
            csvWriterOutputObj
              .writeRecords(responseData.data, {
                append: true,
              })
              .then(() => {
                // console.log("Updated user records");
                b2.increment();
              })
              .catch((err) => {
                console.log(err);
              });
          }
        }
      }
    });

    // Loop through working tree
    for (let i = 0; i < workingTreeRecords.length; i++) {
      const currentRecord = workingTreeRecords[i];

      try {
        const con = await page.waitForSelector(".search-nickname");
        await con.click();

        await page.focus(".search-nickname");
        await page.keyboard.type(currentRecord.combination);

        await page.waitForNetworkIdle({
          timeout: 8000,
        });

        await page.$eval(
          "div.search-nickname > div > input",
          (e) => (e.value = "")
        );

        // Find index of record
        const index = allCsvRecords.findIndex((item) => {
          return item.combination === currentRecord.combination;
        });
        // Update record
        allCsvRecords[index].status = "done";
        b1.increment();
      } catch (error) {
        // Find index of record
        const index = allCsvRecords.findIndex((item) => {
          return item.combination === currentRecord.combination;
        });
        // Update record
        allCsvRecords[index].status = "error";
        b1.increment();
      }
    }

    console.log("Total combinations: ", allCsvRecords.length);
    console.log("Total results/outputs: ", results.length);
    console.log("Working tree length: ", workingTreeRecords.length);

    // Update combination records
    csvWriterObj
      .writeRecords(allCsvRecords)
      .then(() => {
        console.log("Updated combination records");
        multibar.stop();
        process.exit(1);
      })
      .catch((err) => {
        console.log(err);
      });

    // await browser.close();
  } catch (error) {
    console.log(error);
    // Create a log record
    fs.appendFileSync(
      path.join(__dirname, "error.log"),
      `Date: ${new Date().toUTCString()}, Error: ${error.message}\n`,
      (err) => {
        if (err) {
          console.log(err);
        }
      }
    );
  }
}
